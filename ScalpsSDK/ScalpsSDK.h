//
//  AlpsSDK.h
//  AlpsSDK
//
//  Created by Rafal Kowalski on 22.09.16.
//  Copyright © 2016 Alps. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AlpsSDK.
FOUNDATION_EXPORT double AlpsSDKVersionNumber;

//! Project version string for AlpsSDK.
FOUNDATION_EXPORT const unsigned char AlpsSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Alps/PublicHeader.h>
