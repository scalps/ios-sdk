//
//  Utils.swift
//  Alps
//
//  Created by Rafal Kowalski on 04/10/2016
//  Copyright © 2016 Alps. All rights reserved.
//

import Foundation

func now() -> Int64 { return Int64(Date().timeIntervalSince1970 * 1000) }
