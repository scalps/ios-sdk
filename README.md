


# Alps iOS SDK


## Example

To run the example project, clone the repo, and run \`pod install\` from
the Example directory first.


## Requirements


## Installation

Alps is available through [CocoaPods](http://cocoapods.org) it, simply add the following
line to your Podfile:

    pod "AlpsSDK", :git => 'https://bitbucket.org/alps/ios-sdk.git, :tag => 'v0.0.2'


## Author

rk, rafal.kowalski@mac.com


## License

Alps is available under the MIT license. See the LICENSE file for more info.
